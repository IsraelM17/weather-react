import React, {Fragment, useEffect, useState} from 'react';
import Header from "./components/Header";
import Formulario from "./components/Formulario";
import Clima from "./components/Clima";
import Error from "./components/Error";

function App() {

    //state del formulario
    const [busqueda, guardarBusqueda] = useState({
        ciudad  : '',
        pais    : ''
    });

    const [ error, guardarError ]           = useState(false);
    const [resultado, guardarResultado]     = useState({});
    const [ consulta, guardarConsultar ]    = useState(false);

    const { ciudad, pais }  = busqueda;

    useEffect(() =>{
        const consultarAPI = async () => {

            if(consulta){
                const appId = '5bd9f2ec409015f4ac0435caa0d41979';
                const url   = `https://api.openweathermap.org/data/2.5/weather?q=${ciudad},${pais}&appid=${appId}`;

                const respuesta = await fetch(url);
                const resultado = await respuesta.json();

                guardarResultado(resultado);
                guardarConsultar(false);

                if(resultado.cod === '404'){
                    guardarError(true);
                } else
                    guardarError(false);

            }
        };
        consultarAPI();
        // eslint-disable-next-line
    }, [consulta]);

    let componente;
    if(error)
        componente = <Error
            mensaje = "No se encontro la ciudad"
        />;
    else
        componente = <Clima
            resultado={resultado}
        />

  return (
    <Fragment>
        <Header
            titulo = { "Clima React App" }
        />
        <div className="contenedor-form">
            <div className="container">
                <div className="row">
                    <div className="col m6 s12">
                        <Formulario
                            busqueda = { busqueda }
                            guardarBusqueda={guardarBusqueda}
                            guardarConsultar={guardarConsultar}
                        />
                    </div>
                    <div className="col m6 s12">
                        { componente }
                    </div>
                </div>
            </div>
        </div>
    </Fragment>
  );
}

export default App;
